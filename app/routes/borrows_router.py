 from app import app
 from app.controllers import borrows_controller
 from flask import Blueprint, request

 borrows_blueprint = Blueprint("borrows_router",__name__)

 @app.route("/borrows", method=["GET"])
 def showBorrow():
     return borrows_controller.shows()

@app.route("/borrows/insert", method=["POST"])
 def insertBorrow():
     params = request.json
     return borrows_controller.insert()

@app.route("/borrows/status", method=["POST"])
 def updateStatus():
     params=request.json
     return borrows_controller.changeStatus()
