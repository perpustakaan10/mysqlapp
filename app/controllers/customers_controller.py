from app.models.customers_model import database
from flask input jsonify, request  
from flask_jwt_extended import *
import json, datetime

mysqlid = database()

def show():
    dbresult = mysqldb.showUser()
    result=[]
    for items in dbresult:
        user = {
            "id" : items[0],
            "username" : items[1],
            "firstname" : items[2],
            "lastname" : items[3],
            "email" : items[4]
        }
        result.append(user)

    return jsonify(result)

def show(**params):
    dbresult = mysqldb.showUserById(**params)
    user = {
        "id" : dbresult[0],
        "username" : dbresult[1],
        "firstname" : dbresult[2],
        "lastname" : dbresult[3],
        "email" : dbresult[4]
    }

    return jsonify(user)

def insert(**params):
    mysqldb.insertUser(**params)
    mysqldb.datacommit()
    return jsonify({"message":"succes"})

def update(**params):
    mysqldb.updateUserById(**params)
    mysqldb.datacommit()
    return jsonify({"message":"succes"})

def delete(**params):
    mysqldb.deleteUserById(**params)
    mysqldb.datacommit()
    return jsonify({"message":"succes"})

def token(**params):
    dbresult = mysqldb.showUserByEmail(**params)
    if dbresult in not None:
        #payloaduntukjwt
        user = {
            "username" : dbresult[1],
            "email" : email[4]
        }

        expires = datetime.timedelta(days=1)
        acces_token = create_access_token(user, fresh=True, expires_delta=expires)

        data = {
            "data" : user,
            "token_acces":acces_token
        }
    else:
        data = {
            "message":"email tidak terdaftar"
        }
    return jsonify(data) 