from flask import Flask 
from config import Config
from flask_jwt_extended import JWTManager

app = Flask(_name_)
app.config.from_object(Config)
jwt = JWTManager(app)

from app.routers.customers_blueprint import *
from app.routers.borrows_blueprint import *

app.register_blueprint(customers_blueprint)
app.register_blueprint(borrows_blueprint)
